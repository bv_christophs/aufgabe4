#include "image-io.h"
extern void PreMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);
extern void mittelwertFilter(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int size, int gewichtung);
extern void hist(unsigned char INimg[MAXXDIM][MAXYDIM], int out[PIXEL_DP]);
extern void showHist(int in [PIXEL_DP]);
extern int hist_Get_Max_value(int in[PIXEL_DP]);
extern int hist_Get_Min_value(int in[PIXEL_DP]);
extern int hist_Get_Max_index(int in[PIXEL_DP]);
extern int hist_Get_Min_index(int in[PIXEL_DP]);
extern int hist_Get_Border_High(int in[PIXEL_DP]);
extern int hist_Get_Border_Low(int in[PIXEL_DP]);




extern void grauwert_Dehnung(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int hist1[PIXEL_DP], int hist2[PIXEL_DP]);
extern void hist_ebnung_aprox(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int hist1[PIXEL_DP], int hist2[PIXEL_DP], int anz);