#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "pre_processing.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <string>


void PreMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	int myHistogram1[PIXEL_DP];
	int myHistogram2[PIXEL_DP];
	char eingabe = '0';
	bool exit = false;

	do {
		int anz = 0;
		int anz1 = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Histogramm\n");

		printf("2 = Grauwert Dehnung \n");

		printf("3 = Histogramebnung Annaehrung\n");

		printf("4 = \n");

		printf("5 = \n");

		printf("6 = \n");

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);

		printf("\n");

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':
			system("cls");
			printf("====================Histogramm============================\n");
			int myHist[PIXEL_DP];
			hist(inImg,myHist);
			
			printf("Das maximum ist %i\n", hist_Get_Max_value(myHist));
			printf("Das minimum ist %i\n", hist_Get_Min_value(myHist));
			showHist(myHist);
			printf("=======================================================\n");
			printf("Vorgang abgeschlossen!!! \nBitte dueken sie eine belibige taste um in das Menue zurueckzugelangen\n");
			_getch();
			break;
		case '2':
			system("cls");
			printf("====================Grauwertdehnung============================\n");

			grauwert_Dehnung(inImg, outImg,myHistogram1,myHistogram2);
			
			system("cls");
			printf("Das Histogramm des originalbildes wurde erstellt\nund kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
			_getch();
			showHist(myHistogram1);
			printf("\n\n========================================================================\n");

			system("cls");
			printf("Das Histogramm des bearbeitenden Bildes wurde erstellt\nund kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
			_getch();
			
			showHist(myHistogram2);
			printf("\n\n========================================================================\n");
			system("cls");
			printf("Die Bearbeitung wurde durchgefuehrt\nund das Ausgabe Bild kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
			_getch();
			writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
			//My_IMGDEBUG(outImg);
			printf("\n\n========================================================================\n");




			break;
		case '3':
			system("cls");
			printf("====================Histogrammebnung Annaehrung============================\n");
			printf("Wie viele Grauwertstufen soll ihr Histogramm enthalten ? :");
			scanf("%i", &anz);

			if (anz > 0)
			{
				hist_ebnung_aprox(inImg, outImg,myHistogram1,myHistogram2,anz);
				
				
				printf("=======================================================\n");
			}
			else
			{
				system("cls");
				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");
				_getch();
			}




			
			break;

		case '4':
			system("cls");
			printf("====================Mittelwert Filter ============================\n");
			printf("Welche gr��e soll der filter Haben? :");
			scanf("%i", &anz);
			printf("\nWelche Gewichtung soll der filter haben ? :");
			scanf("%i", &anz1);


			if (anz > 0)
			{
				

				mittelwertFilter(inImg, outImg, anz, anz1);

				printf("=======================================================\n");
			}
			else
			{
				system("cls");
				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");
				_getch();
			}

			break;
		case '5':
		
			break;

		case '6':
	
			break;
		}





	} while (exit == false);


}


// Bild restauration Gl�ttung / rauschunterdr�ckung
void mittelwertFilter(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM] ,int size,int gewichtung )
{
	// gucken ob die filter gerade oder ungerade sind
	bool evenFilter = false;
	if (size % 2 != 0)
	{
		evenFilter = false;

	}
	else
	{
		evenFilter = true;
	}

	
	if (evenFilter == true)
	{
		initImmageWhite(outImg);
	
		// Gerader Mittelwertfilter 
		int startPunkt = (size / (int)2);

		for (int x = startPunkt - 1;x < MAXXDIM - startPunkt ;x++)
		{
			for (int y = startPunkt - 1;y <MAXYDIM - startPunkt ;y++)
			{
				
			}
		}

		My_IMGDEBUG(outImg);
	}

	if (evenFilter == false)
	{
		// Ungerader Mittelwertfilter
		initImmage(outImg);
		int startPunkt = (size / (int)2 +1);
		int test = 0;
		for (int x = startPunkt -1 ;x < MAXXDIM - (startPunkt-1)  ;x++)
		{
			for (int y = startPunkt - 1;y < MAXYDIM - (startPunkt - 1);y++)
			{
				
				int value = 0;
				for (int x1  = x-size/ (int)2;x1<x + size/(int)2 +1; x1++)
				{
					
					for (int y1 = y - size/(int)2;y1<y +(size / (int)2)+1;y1++)
					{
						
						if (x == x1 && y == y1)
						{
							value = value + gewichtung * inImg[x1][y1];
							
						}
						else
						{
							value = value + inImg[x1][y1];

						}
					}
				}


				float outvalue =((float)value / ((float)size * (float)size));

				if (outvalue < 256)
				{
					outImg[x][y] = (float)outvalue;
				}
				else
				{
					outImg[x][y] = 255;
				}
			}	
		}

		My_IMGDEBUG(outImg);
	}

	_getch();

}



// Bild restauration Histogramm

void hist(unsigned char INimg[MAXXDIM][MAXYDIM], int out[PIXEL_DP])
{
	for (int i = 0; i < PIXEL_DP ;i++)
	{
		out[i] = 0;
	}

	for (int x = 0; x < MAXXDIM ; x++)
	{
		for (int y = 0; y < MAXYDIM ; y++)
		{

			out[INimg[x][y]] ++;

		}
	}

}

int hist_Get_Max_value(int in[PIXEL_DP])
{
	int max_value = 0;
	for (int i = 0; i < PIXEL_DP;i++)
	{
		if (in[i] > max_value)
		{
			max_value = in[i];
		}
	}
	return max_value;
}

int hist_Get_Min_value(int in[PIXEL_DP])
{
	int min_value = PIXEL_DP;
	for (int i = 0; i < PIXEL_DP;i++)
	{
		if (in[i] < min_value)
		{
			min_value = in[i];
		}
	}
	return min_value;
}


int hist_Get_Max_index(int in[PIXEL_DP])
{
	int max_value = 0;
	int max_index = 0;
	for (int i = 0; i < PIXEL_DP;i++)
	{
		if (in[i] > max_value)
		{
			max_value = in[i];
			max_index = i;
		}
	}
	return max_index;
}

int hist_Get_Min_index(int in[PIXEL_DP])
{
	int min_value = PIXEL_DP;
	int min_index = 0;
	for (int i = 0; i< PIXEL_DP;i++)
	{
		if (in[i] < min_value && in[i] > 0 )
		{
			min_value = in[i];
			
			min_index = i;
			
		}
	}
	return min_index;
}


int hist_Get_Border_Low(int in[PIXEL_DP])
{
	int borderLow = 256;
	
	for (int i = PIXEL_DP; i > 0;i--)
	{
		
		if (in[i] > 0)
		{

			borderLow = i;

		}
	}
	return borderLow;
}

int hist_Get_Border_High(int in[PIXEL_DP])
{
	int borderHigh = 0;

	for (int i = 0; i< PIXEL_DP;i++)
	{
		if (in[i] > 0)
		{

			borderHigh = i;

		}
	}
	return borderHigh;
}




void showHist(int in [PIXEL_DP])
{
	unsigned char hist_Immage[MAXXDIM][MAXYDIM];

	initImmageWhite(hist_Immage);

	int max = hist_Get_Max_value(in);


	
	float faktor = (float)PIXEL_DP / max;


	for (int y = 0; y < MAXYDIM;y++)
	{
		for (int x = 255; x > (int)((float)255 -((float) in[y] *faktor));x--)
		{
			hist_Immage[x][y] = 0;
		}

	}

	//My_IMGDEBUG(hist_Immage);
	writeImage_ppm(hist_Immage, MAXXDIM, MAXYDIM);
}

void grauwert_Dehnung (unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM],  int hist1[PIXEL_DP], int hist2[PIXEL_DP])
{
	
	hist(inImg, hist1);
	
	int border_min = hist_Get_Border_Low(hist1);
	int border_max = hist_Get_Border_High(hist1);
	float faktor = (float)255 / ((float)border_max - (float)border_min);
	
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = (int)(faktor * (float)(inImg[x][y] - border_min));
		}
	}
	hist(outImg,hist2);
}
// Diese funktion Berechnet eine ann�hrung an die Histogrammebnung Gemaes 
void hist_ebnung_aprox(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int hist1[PIXEL_DP], int hist2[PIXEL_DP],int anz)
{

	
	
	hist(inImg, hist1);
	system("cls");
	printf("Das Histogramm des originalbildes wurde erstellt\nund kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
	_getch();
	showHist(hist1);
	printf("\n\n========================================================================\n");

	// Kumatives Histogramm berechnen
	for (int i = 1 ; i< PIXEL_DP;i++)
	{
		hist1[i] = hist1[i - 1] + hist1[i];
	}
	
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = (hist1[inImg[x][y]])*(anz - 1) / (MAXXDIM*MAXYDIM);
		}
	}

	hist(outImg, hist2);

	
	system("cls");
	printf("Das Histogramm des bearbeitenden Bildes wurde erstellt\nund kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
	_getch();
	hist(outImg, hist2);
	showHist(hist2);
	printf("\n\n========================================================================\n");
	system("cls");
	printf("Die Bearbeitung wurde durchgefuehrt\nund das Ausgabe Bild kann nun Abgespeichert werden!!!\ndruecken sie eine belibige taste um dises abzuspeichern");
	_getch();
	writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
	//My_IMGDEBUG(outImg);
	printf("\n\n========================================================================\n");
}

