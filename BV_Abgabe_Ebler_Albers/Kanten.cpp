#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "pre_processing.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <string>
#include<cmath>
#include "Kanten.h"

void KantenMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	
	char eingabe = '0';
	bool exit = false;

	do {
		int anz = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Sobel X\n");

		printf("2 = Sobel Y\n");

		printf("3 = Sobel X Y \n");

		printf("4 = Laplace \n");

		printf("5 = \n");

		printf("6 = \n");

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);


		printf("\n");
		int sobXY[MAXXDIM][MAXYDIM];
		int sobX[MAXXDIM][MAXYDIM];
		int sobY[MAXXDIM][MAXYDIM];

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':
			
			
			
			SobelX(inImg, sobX);
			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobX[x][y] != 0)
					{
						outImg[x][y] =255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;
		case '2':
			
			
			SobelY(inImg, sobY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobY[x][y] != 0)
					{
						outImg[x][y] = 255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);


			break;
		case '3':
			

			SobelXY(inImg,sobX,sobY ,sobXY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobXY[x][y] != 0)
					{
						outImg[x][y] = 255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;

		case '4':
			initImmageGray(outImg);

			laplace(inImg, sobXY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobXY[x][y] >0)
					{
						outImg[x][y] = 255;
					}
					if (sobXY[x][y] < 0)
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;
		case '5':

			break;

		case '6':

			break;
		}





	} while (exit == false);


}

void SobelX(unsigned char inImg[MAXXDIM][MAXYDIM],int outImg[MAXXDIM][MAXYDIM])
{
	
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y]  = 1 * inImg[x - 1][y - 1] + -1 * inImg[x - 1][y + 1]
				+ 2 * inImg[x][y - 1] + -2 * inImg[x][y + 1]
				+ 1 * inImg[x + 1][y - 1] + -1 * inImg[x + 1][y + 1];

			
			 
		}
	}
}

void SobelY(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM])
{

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y] = 1 * inImg[x - 1][y - 1] + 2 * inImg[x - 1][y] + 1 * inImg[x - 1][y + 1] +
				          -1 * inImg[x + 1][y - 1] - 2 * inImg[x + 1][y] - 1 * inImg[x + 1][y + 1];
				


		}
	}
}

void SobelXY(unsigned char inImg[MAXXDIM][MAXYDIM], int sobX[MAXXDIM][MAXYDIM], int sobY[MAXXDIM][MAXYDIM],int out[MAXXDIM][MAXYDIM])
{
	

	

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			
			out[x][y] = 0;

		}
	}


	SobelX(inImg, sobX);
	SobelY(inImg, sobY);

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			out[x][y] = (int)pow((pow(sobX[x][y], 2)+ pow(sobY[x][y], 2)),0.5);
		}
	}

}


void laplace(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM])
{

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y] = 1*inImg[x-1][y]+ 1 * inImg[x ][y-1] + 1 * inImg[x][y + 1] + 1 * inImg[x +1][y ]  -4 * inImg[x][y];
		}
	}
}