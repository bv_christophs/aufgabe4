#include "image-io.h"
extern void KantenMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);
extern void SobelX(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM]);
extern void SobelY(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM]);
//extern void SobelXY(unsigned char inImg[MAXXDIM][MAXYDIM],  int out[MAXXDIM][MAXYDIM]);
extern void SobelXY(unsigned char inImg[MAXXDIM][MAXYDIM], int sobX[MAXXDIM][MAXYDIM], int sobY[MAXXDIM][MAXYDIM], int out[MAXXDIM][MAXYDIM]);
extern void laplace(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM]);