//#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <stdlib.h>
#include "image-io.h"

int readImage_ppm(unsigned char img[MAXXDIM][MAXYDIM])
{
	char	imagedir[128]="C:\\bv\\";
	char	fname[30];
	FILE	*fpimage;
	char	z1[3];
	char    z2[24];
	int		xdim,ydim,type,i,j,x,y;
	int		r,g,b;

    char	dirbvdir[256]="dir C:\\bv\\*.ppm /B";

 	system(dirbvdir);

	printf("--------------------\nBild lesen::Dateiname (ohne '.ext'!) : ");
	scanf("%s",&fname);
	strcat(fname,".ppm");
	strcat(imagedir,fname);

	if ((fpimage = fopen(imagedir,"r")) == NULL) {
		printf("Kann Datei <%s> nicht oeffnen!\n",imagedir);
		return 1;
	} else {
		// Header lesen ...
		fgets(z1,3,fpimage);
		fgets(z2,24,fpimage);
		fseek(fpimage, 26, SEEK_SET);
		fscanf(fpimage,"%d",&xdim);
		fscanf(fpimage,"%d",&ydim);
		fscanf(fpimage,"%d",&type);

		printf("xdim = %d\tydim = %d\ttype = %d\n",xdim,ydim,type);

		printf("Lese Daten aus Datei: <%s>\n",imagedir);
		// Bilddaten lesen ...
			for (i=0; i < MAXXDIM;i++){
				for (j=0; j < MAXYDIM;j++){
					fscanf(fpimage,"%d",&r);
					fscanf(fpimage,"%d",&g);
					fscanf(fpimage,"%d",&b);
					img[i][j] = g;
				}
			}
	}
	fclose(fpimage);

	return 0;
}

int writeImage_ppm(unsigned char img[MAXXDIM][MAXYDIM], int xdim, int ydim)
{
	char	imagedir[128]="C:\\bv\\";
	char	fname[30];
	FILE	*fpimage;
	int		i,j;
	int		type=255;

	char	dirbvdir[256]="dir C:\\bv\\*.ppm /B";

 	system(dirbvdir);

	printf("--------------------\nBild speichern::Dateiname (ohne '.ext'!) : ");
	scanf("%s",&fname);
	strcat(fname,".ppm");
	strcat(imagedir,fname);

	if ((fpimage = fopen(imagedir,"w+")) == NULL) {
		printf("Kann Datei <%s> nicht oeffnen!\n",imagedir);
		return 1;
	} else {
		fprintf(fpimage,"P3\n");
		fprintf(fpimage,"# Created by IrfanView\n");
		fprintf(fpimage,"%d %d\n",xdim,ydim);
		fprintf(fpimage,"%d\n",type);
	
		for (i=0;i<xdim;i++){
			for (j=0;j<ydim;j++){
				fprintf(fpimage,"%d %d %d ",img[i][j], img[i][j], img[i][j]);
			}
		}
	}
	fclose(fpimage);

	return 0;
}

//
// es wird das externe Programm irfanview als Viewer verwendet
// die Bilder muessen im Verzeichnis C:\bv\ gespeichert sein!
//
void viewImage_ppm(void)
{
	char	imagedir[128]="C:\\bv\\";
	char	viewer[256]="C:\\bv\\i_view32.exe ";
	char	fname[30];
	char	dirbvdir[256]="dir C:\\bv\\*.ppm /B";

 
	system(dirbvdir);

	printf("--------------------\nBildanzeige::Dateiname (ohne '.ext'!) : ");
	scanf("%s",&fname);
	strcat(fname,".ppm");
	strcat(imagedir,fname);
	strcat(viewer,imagedir);

	system(viewer);	 
}

void initImmage(unsigned char INimg[MAXXDIM][MAXYDIM])
{
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			INimg[x][y] = 0;

		}
	}

}

void initImmageWhite(unsigned char INimg[MAXXDIM][MAXYDIM])
{
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			INimg[x][y] = 255;

		}
	}

}

void initImmageGray(unsigned char INimg[MAXXDIM][MAXYDIM])
{
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			INimg[x][y] = 128;

		}
	}

}

void copyImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM])
{
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			OUTimg[x][y] = INimg[x][y];

		}
	}
}



////////////////////////////////------------------ Modifizierte Funktionen -------------------------------------------/////////////////////////////

//Chritoph Ebler
//51411
//03.10.2017

// Modifizierte funktion mit dem dateinamen im argument 
//�ffnet die entsprechende Datei
void MY_viewImage_ppm(char	fname[30])
{
	char	imagedir[128] = "C:\\bv\\";
	char	viewer[256] = "C:\\bv\\i_view32.exe ";
	
	char inFiName[30];
	strcpy(inFiName, fname);
	strcat(inFiName, ".ppm");
	strcat(imagedir, inFiName);
	strcat(viewer, imagedir);

	system(viewer);
}

// Modifizierte funktion mit dem dateinamen im argument 
//liesst die dati n�n matrix ein 
int My_readImage_ppm(char Filename[30],unsigned char img[MAXXDIM][MAXYDIM])
{
	char	imagedir[128] = "C:\\bv\\";
	char	fname[30];
	FILE	*fpimage;
	char	z1[3];
	char    z2[24];
	int		xdim, ydim, type, i, j, x, y;
	int		r, g, b;


	strcpy(fname, Filename);
	strcat(fname, ".ppm");
	strcat(imagedir, fname);

	if ((fpimage = fopen(imagedir, "r")) == NULL) {
		printf("Kann Datei <%s> nicht oeffnen!\n", imagedir);
		return 1;
	}
	else {
		// Header lesen ...
		fgets(z1, 3, fpimage);
		fgets(z2, 24, fpimage);
		fseek(fpimage, 26, SEEK_SET);
		fscanf(fpimage, "%d", &xdim);
		fscanf(fpimage, "%d", &ydim);
		fscanf(fpimage, "%d", &type);

		printf("xdim = %d\tydim = %d\ttype = %d\n", xdim, ydim, type);

		printf("Lese Daten aus Datei: <%s>\n", imagedir);
		// Bilddaten lesen ...
		for (i = 0; i < MAXXDIM;i++) {
			for (j = 0; j < MAXYDIM;j++) {
				fscanf(fpimage, "%d", &r);
				fscanf(fpimage, "%d", &g);
				fscanf(fpimage, "%d", &b);
				img[i][j] = g;
			}
		}
	}
	fclose(fpimage);

	return 0;
}

// Modifizierte funktion mit dem dateinamen im argument 
//Sprichert die Matrix unter dem entsprechenden namen

int My_writeImage_ppm(char Filename[30],unsigned char img[MAXXDIM][MAXYDIM], int xdim, int ydim)
{
	char	imagedir[128] = "C:\\bv\\";
	char	fname[30];
	FILE	*fpimage;
	int		i, j;
	int		type = 255;

	
	strcpy(fname, Filename);
	strcat(fname, ".ppm");
	strcat(imagedir, fname);

	if ((fpimage = fopen(imagedir, "w+")) == NULL) {
		printf("Kann Datei <%s> nicht oeffnen!\n", imagedir);
		return 1;
	}
	else {
		fprintf(fpimage, "P3\n");
		fprintf(fpimage, "# Created by IrfanView\n");
		fprintf(fpimage, "%d %d\n", xdim, ydim);
		fprintf(fpimage, "%d\n", type);

		for (i = 0;i<xdim;i++) {
			for (j = 0;j<ydim;j++) {
				fprintf(fpimage, "%d %d %d ", img[i][j], img[i][j], img[i][j]);
			}
		}
	}
	fclose(fpimage);

	return 0;
}

void getFilename(char fname[30])
{
	char inFname[30];
	char	imagedir[128] = "C:\\bv\\";
	printf("--------------------\n Quell name des Quell bildes::Dateiname (ohne '.ext'!) : ");
	scanf("%s", &inFname);
	//strcat(inFname, ".ppm");
	strcat(imagedir, inFname);
	strcpy(fname, inFname);
}


void My_IMGDEBUG(unsigned char img[MAXXDIM][MAXYDIM])
{
	char fname[30] = "eblersSuperDebug";

	My_writeImage_ppm(fname, img, MAXXDIM, MAXYDIM);
	MY_viewImage_ppm(fname);
}
